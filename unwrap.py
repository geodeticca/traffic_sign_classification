import torch

state_dict = torch.load("model/regular_model_resnet34_ep-100_6_v2.pth", map_location=torch.device('cpu'))
for key in list(state_dict.keys()):
    state_dict[key.replace('model.', '')] = state_dict.pop(key)

torch.save(state_dict, "model/regular_model_resnet34_ep-100_6_v2.pth")