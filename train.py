import os
os.environ["CUDA_VISIBLE_DEVICES"]="1"
import timm
from PIL import Image, ImageFile, ImageEnhance
ImageFile.LOAD_TRUNCATED_IMAGES = True
import torch
import wandb
import torchvision
import random
from torch import nn, optim
import torch.nn.functional as F
from fastai.vision.all import *
from torch.utils.data import ConcatDataset
from pathlib import Path
from torchvision.transforms import Compose, Resize, RandomCrop, RandomChoice, RandomEqualize, CenterCrop, ToTensor, Pad, Lambda, RandomRotation, ToPILImage, RandomAutocontrast, RandomAdjustSharpness, RandomPerspective
import pandas as pd
from skimage import io, transform
import numpy as np
import matplotlib.pyplot as plt
from torch.utils.data import Dataset, DataLoader
from torchvision import transforms, utils
from torchvision.datasets import ImageFolder
import json
from dataset import *
from pytorch_lightning import Trainer, seed_everything
from tqdm.notebook import tqdm, trange
import pytorch_lightning as pl
from pytorch_lightning.loggers import WandbLogger
import torchmetrics
from model import RegularModel


class LitAuto(pl.LightningModule):
    def __init__(self):
        super().__init__()
        self.model = regular_model
        self.accuracy = torchmetrics.Accuracy()
        self.f1 = torchmetrics.F1Score(num_classes=len(ds))
    def forward(self, x):
        return self.model(x)

    def configure_optimizers(self):
        optimizer = torch.optim.Adam(self.parameters(), lr=1e-3)
        return optimizer

    def training_step(self, batch, batch_idx):
        x, y = batch
        y_hat = self(x)
        loss = F.cross_entropy(y_hat, y)
        self.accuracy(y_hat.detach().softmax(dim=-1), y)
        self.f1(y_hat.detach().softmax(dim=-1), y)
        self.log("train_acc", self.accuracy, on_epoch=True)
        self.log("train_f1", self.f1, on_epoch=True)
        self.log('train_loss', loss.detach().cpu().item(), on_step=True, on_epoch=True, prog_bar=True, logger=True)
        return loss
    
    def validation_step(self, batch, batch_idx):
        x, y = batch
        y_hat = self(x)
        loss = F.cross_entropy(y_hat, y)
        self.accuracy(y_hat.softmax(dim=-1), y)
        self.f1(y_hat.detach().softmax(dim=-1), y)
        self.log("val_acc", self.accuracy, on_epoch=True, prog_bar=True)
        self.log("val_f1", self.f1, on_epoch=True)
        self.log("val_loss", loss.detach().cpu().item(), on_step=True, on_epoch=True, prog_bar=True, logger=True)
    
    def val_dataloader(self):
        return torch.utils.data.DataLoader(test_ds, batch_size=62, shuffle=False, num_workers=8)
    
    def train_dataloader(self):
        return torch.utils.data.DataLoader(hallucinator_ds, batch_size=128, shuffle=False, num_workers=8)

seed_everything(42, workers=True)

ds = TrafficSign("./data/traffic_sign_png_filtered", transform=Compose([ RandomChoice([Resize((128,128)), Resize((112,112)), Resize((100,100)), Resize((84,84))], p=[0.4, 0.5,0.5, 0.6]), RandomRotation(5), RandomPerspective(distortion_scale=0.5)]))

test_ds = TrafficSignGT("data/gt_labeled", label_map=ds.label_map, transform=Compose([Resize((128, 128)), ToTensor(), Normalize(mean = [0.4944, 0.4969, 0.5045], std = [0.2375, 0.2468, 0.2704] )]))

bg = NoSign("./data/nosign_1", transform=Compose([Resize((1024)), RandomCrop((184, 184))]))

hallucinator_ds = TrafficHallucinator(ds, bg, transform=Compose([Resize((128, 128)), RandomAdjustSharpness(sharpness_factor=2), RandomEqualize(p=0.2), RandomAdjustSharpness(0.5, p=0.5), RandomAutocontrast(p=0.5), ToTensor(), Normalize(mean = [0.4944, 0.4969, 0.5045], std = [0.2375, 0.2468, 0.2704] )]))

hallucinator_loader = torch.utils.data.DataLoader(hallucinator_ds, batch_size=62, shuffle=False, num_workers=8)

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
model = PrototypicalNetwork("resnet34")
optimizer = torch.optim.Adam(model.parameters(), lr=1e-3)
model.to(device)
model.train()
regular_model = RegularModel(model, len(ds)).cuda()

pl_model = LitAuto()
wandb_logger = WandbLogger(project="traffic_signs_hallucination_v2", name="with_augmentation", log_model=True)
wandb_logger.experiment.config["model"] = "resnet34"

trainer = pl.Trainer(gpus=-1, logger=wandb_logger, max_epochs=100)
trainer.fit(pl_model)
torch.save(pl_model.state_dict(), "model/regular_model_resnet18_ep-100_with_augmentation_v2.pth")
wandb.finish()
