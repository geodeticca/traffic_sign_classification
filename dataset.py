import os
import torch
from torch.utils.data import Dataset
from PIL import Image, ImageFile, ImageEnhance

ImageFile.LOAD_TRUNCATED_IMAGES = True

def post_mapping(key):
    remap = {
        '045': ['045', '046'],
        '047': ['047', '048'],
        '049': ['049', '050'],
        '051': ['051', '052'],
        '053': ['053', '054'],
        '055': ['055', '056'],
        '057': ['057', '058'],
        '059': ['059', '060'],
        '114': ['113', '114', '117', '118', '119', '120', '121', '122', '123', '124', '125', '126', '127', '128',
                '129'],
        '614': ['614', '615'],
        '630': ['630', '631'],
        '632': ['632', '633'],
        '634': ['634', '635'],
        '453': ['453', '454', '455', '456'],
        '341': ['341', '342'],
        '324': ['325']
    }
    for k, v in remap.items():
        if key in v:
            return k
    return key


class TrafficSign(Dataset):

    def __init__(self, root_dir, transform=None):
        """
        Args:
            root_dir (string): Directory with all the images.
            transform (callable, optional): Optional transform to be applied
                on a sample.
        """
        self.root_dir = root_dir
        self.files = [f for f in os.listdir(self.root_dir) if os.path.isfile(os.path.join(self.root_dir, f))]
        self.label_map = {post_mapping(str(f).split(".svg.png")[0]): i for i, f in enumerate(self.files)}
        self.transform = transform
        self.cat_dict = {}

    def __len__(self):
        return len(self.files)

    def __getitem__(self, idx):
        if torch.is_tensor(idx):
            idx = idx.tolist()
        img_name = os.path.join(self.root_dir,
                                self.files[idx])

        image = Image.open(img_name)
        label = self.label_map[post_mapping(str(self.files[idx]).split(".svg.png")[0])]
        if self.transform:
            image = self.transform(image)
        return image, label

class TrafficHallucinator(IterableDataset):

    def __init__(self, traffic_sign_ds, no_sign_ds, transform=None, min_contrast_factor = 0.11):
        """
        Args:
            root_dir (string): Directory with all the images.
            transform (callable, optional): Optional transform to be applied
                on a sample.
        """
        self.tds = traffic_sign_ds
        self.nds = no_sign_ds
        self.transform = transform
        self.ltds = len(self.tds)
        self.lnds = len(self.nds)
        self.min_contrast_factor = min_contrast_factor
   
    def __iter__(self):
        for img, label in self.tds:
            bg = random.choice(self.nds).convert("RGBA")
            img = img.convert("RGBA")
            enhancer = ImageEnhance.Contrast(img)
            img = enhancer.enhance(max(self.min_contrast_factor, np.random.random()))
            bw, bh = bg.size
            iw, ih = img.size
            ow = int(np.random.random() * (bw-iw) )
            oh = int(np.random.random() * (bh-ih) )
            offset = (ow, oh)
            bg.paste(img, offset, img)
            bg = bg.convert("RGB")
            if self.transform is not None:
                bg = self.transform(bg)
            yield bg, label
            
class NoSign(Dataset):
    def __init__(self, root_dir, transform=None, ext=".jpg"):
        """
        Args:
            root_dir (string): Directory with all the images.
            transform (callable, optional): Optional transform to be applied
                on a sample.
        """
        self.root_dir = root_dir
        self.files = [ f for f in os.listdir(self.root_dir) if os.path.isfile(os.path.join(self.root_dir, f))]
        self.transform = transform
        self.ext = ext
        
    def __len__(self):
        return len(self.files)

    def __getitem__(self, idx):
        if torch.is_tensor(idx):
            idx = idx.tolist()
        img_name = os.path.join(self.root_dir,
                                self.files[idx])
        
        image = Image.open(img_name)
        if self.transform:
            image = self.transform(image)
        return image
    
class TrafficSign(Dataset):

    def __init__(self, root_dir, transform=None):
        """
        Args:
            root_dir (string): Directory with all the images.
            transform (callable, optional): Optional transform to be applied
                on a sample.
        """
        self.root_dir = root_dir
        self.files = [ f for f in os.listdir(self.root_dir) if os.path.isfile(os.path.join(self.root_dir, f))]
        self.label_map = { post_mapping(str(f).split(".svg.png")[0]):i for i,f in enumerate(self.files) }
        self.transform = transform
        self.cat_dict = {}
        
    def __len__(self):
        return len(self.files)

    def __getitem__(self, idx):
        if torch.is_tensor(idx):
            idx = idx.tolist()
        img_name = os.path.join(self.root_dir,
                                self.files[idx])
        
        image = Image.open(img_name)
        label = self.label_map[post_mapping(str(self.files[idx]).split(".svg.png")[0])]
        if self.transform:
            image = self.transform(image)
        return image, label
    
class TrafficSignCrops(Dataset):

    def __init__(self, root_dir, transform=None):
        """
        Args:
            root_dir (string): Directory with all the images.
            transform (callable, optional): Optional transform to be applied
                on a sample.
        """
        self.root_dir = root_dir
        self.files = [ f for f in os.listdir(self.root_dir) if os.path.isfile(os.path.join(self.root_dir, f))]
        self.transform = transform
        self.cat_dict = {}
        
    def __len__(self):
        return len(self.files)

    def __getitem__(self, idx):
        if torch.is_tensor(idx):
            idx = idx.tolist()
        img_name = os.path.join(self.root_dir,
                                self.files[idx])
        
        image = Image.open(img_name)
        if self.transform:
            image = self.transform(image)
        return image
    
class TrafficSignShots(Dataset):

    def __init__(self, root_dir, transform=None):
        """
        Args:
            root_dir (string): Directory with all the images.
            transform (callable, optional): Optional transform to be applied
                on a sample.
        """
        self.root_dir = root_dir
        self.data = json.load(open(f"{root_dir}/_list.json.json"))
        self.files = [ f"images/{f['foto']}.png" for f in self.data['modul_pasportdoprava_zdz']]
        self.transform = transform
        self.cat_dict = {}
        
    def __len__(self):
        return len(self.files)

    def __getitem__(self, idx):
        if torch.is_tensor(idx):
            idx = idx.tolist()
        img_name = os.path.join(self.root_dir,
                                self.files[idx])
        image = Image.open(img_name)
        if self.transform:
            image = self.transform(image)
        return image