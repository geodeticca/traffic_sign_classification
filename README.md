* `model/regular_model_resnet18_ep-100_3.pth` trained on 317 classes. val_acc=56% train_Acc=98% val_f1=56% image_resolution=(128,128) normalized
* `model/regular_model_resnet34_ep-100_6_clean.pth` trained on 317 classes. val_acc=58% train_Acc=99% val_f1=58.9% image_resolution=(128,128) normalized
* `model/regular_model_resnet34_ep-100_6_clean.pth` trained on 253 classes. val_acc=50% train_Acc=97% val_f1=50.69% image_resolution=(128,128) normalized

If using torchvision a transform like this will do the job:
`transform=Compose([Resize((128, 128)), ToTensor(), Normalize(mean = [0.4944, 0.4969, 0.5045], std = [0.2375, 0.2468, 0.2704])])`

The label mapping is in label_map.json. It is following the filename in the training data. The data is in the data folder. 

V2 models are the new trained weight with the new training set.

This is the mapping function to fix the redendant labels:
```
def post_mapping(key):
    remap = {
        '045': ['045','046'],
        '047': ['047','048' ],
        '049': ['049','050'],
        '051': ['051','052'],
        '053': ['053','054'],
        '055': ['055','056'],
        '057': ['057','058'],
        '059': ['059','060'],
        '114': ['113','114','117','118','119','120','121','122','123','124','125','126','127','128','129'],
        '614': ['614','615'],
        '630': ['630','631'],
        '632': ['632','633'],
        '634': ['634','635'],
        '453': ['453','454','455','456'],
        '341': ['341','342'],
        '324': ['325']
    }
    for k,v in remap.items():
        if key in v:
            return k
    return key
```