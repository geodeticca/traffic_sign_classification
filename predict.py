from model import PrototypicalNetwork, RegularModel
import torch
from torchvision.transforms import Compose, ToTensor, Normalize, Resize
from PIL import Image

transform = Compose([Resize((128, 128)), ToTensor(), Normalize(mean=[0.4944, 0.4969, 0.5045], std=[0.2375, 0.2468, 0.2704])])
img = Image.open("img/images.png").convert("RGB")
tensor = transform(img)
tensor = tensor.unsqueeze(0) # Create batch dimension

# For resnet18
model = PrototypicalNetwork("resnet18")
regular_model = RegularModel(model, num_classes=317)
state_dict = torch.load("model/regular_model_resnet18_ep-100_3.pth", map_location=torch.device('cpu'))
regular_model.load_state_dict(state_dict, strict=True)
regular_model.eval()
activations = regular_model(tensor)

# For resnet34
model = PrototypicalNetwork("resnet34")
regular_model = RegularModel(model, num_classes=317)
state_dict = torch.load("model/regular_model_resnet34_ep-100_6_clean.pth", map_location=torch.device('cpu'))
regular_model.load_state_dict(state_dict, strict=True)
regular_model.eval()
activations = regular_model(tensor)


# For resnet34 with v2
model = PrototypicalNetwork("resnet34")
regular_model = RegularModel(model, num_classes=253)
state_dict = torch.load("model/regular_model_resnet34_ep-100_6_v2.pth", map_location=torch.device('cpu'))
regular_model.load_state_dict(state_dict, strict=True)
regular_model.eval()
activations = regular_model(tensor)