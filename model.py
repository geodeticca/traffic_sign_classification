import torch.nn as nn
import timm
from fastai.vision.all import LinBnDrop


class PrototypicalNetwork(nn.Module):
    def __init__(self, backbone="conv4"):
        super(PrototypicalNetwork, self).__init__()
        self.encoder = timm.create_model(backbone, num_classes=0, pretrained=False)
    def forward(self, inputs):
        embeddings = self.encoder(inputs.view(-1, *inputs.shape[2:]))
        return embeddings.view(*inputs.shape[:2], -1)
    
class RegularModel(nn.Module):
    def __init__(self, model, num_classes):
        super(RegularModel, self).__init__()
        self.enc = model
        self.lin = LinBnDrop(512, num_classes)
    def forward(self, input):
        emb = self.enc(input.unsqueeze(0))
        return self.lin(emb.squeeze(0))
